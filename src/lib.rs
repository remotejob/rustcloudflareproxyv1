use worker::*;

#[event(fetch, respond_with_errors)]
pub async fn main(req: Request, env: Env, _ctx: worker::Context) -> Result<Response> {
    // utils::set_panic_hook();

    let router = Router::new();

    router
        .post_async("/", |_req, ctx| async move {
            let kv = ctx.kv("workers-kv-from-rust-KV_FROM_RUST")?;

            kv.put("key", "value")?.execute().await?;

            Response::empty()
        })
        .run(req, env).await
}